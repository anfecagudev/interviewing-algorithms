package algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Should return a List containing all the possible indexes combination for array of size provided
 * For example, if i=3, then
 * [{0,1,2}, {0,2,1}, {1,0,2}, {1,2,0}, {2,1,0}, {2,0,1}]
 * Should be the result.
 */
public class ArrayIndexesCombination {
    public static void main(String[] args) {
        int arraySize = 3;
        List<int[]> recursiveResults = createArrayIndexCombinationRecursively(arraySize);
        List<int[]> results = createArrayIndexCombination(arraySize);
        assert recursiveResults.equals(results);
        System.out.println("Recursive and iterative method returned same values");
    }


    public static List<int[]> createArrayIndexCombinationRecursively(int arraySize){
        List<int[]> indexCombinationArray = new ArrayList<>();
        for(int i = 0; i<arraySize; i++){
            int[] arr = new int[arraySize];
            Arrays.fill(arr, -1000);
            arr[0]=i;
            createArrayIndexCombinationRecursively(arr, 1, indexCombinationArray);
        }
        return indexCombinationArray;
    }

    private static void createArrayIndexCombinationRecursively(int[] array, int index, List<int[]> combinationList){
        if(index==array.length){
            combinationList.add(array);
            return;
        }
        for(int i = 0; i<array.length; i++){
            if(validIndexForArray(array, i)){
                int[] newArray = new int[array.length];
                System.arraycopy(array, 0, newArray, 0, array.length);
                newArray[index]=i;
                createArrayIndexCombinationRecursively(newArray, index+1, combinationList);
            }
        }
    }


    public static List<int[]> createArrayIndexCombination(int arraySize) {
        int index = 0;
        List<int[]> indexCombinationArray = new CopyOnWriteArrayList<>();
        while(index < arraySize){
            if(index==0){
                for(int i=0; i<arraySize; i++){
                    int[] newArray = new int[arraySize];
                    Arrays.fill(newArray, -1000);
                    newArray[index]=i;
                    indexCombinationArray.add(newArray);
                }
                index++;
                continue;
            }
            for(int[] array: indexCombinationArray){
                for(int i =0; i<arraySize; i++){
                    if(validIndexForArray(array, i)){
                        int[] newArray = new int[arraySize];
                        System.arraycopy(array, 0, newArray, 0, arraySize);
                        newArray[index]=i;
                        indexCombinationArray.add(newArray);
                    }
                }
                indexCombinationArray.remove(array);
            }
            index++;
        }
        return indexCombinationArray;
    }

    private static boolean validIndexForArray(int[] array, int value){
        for(int checkValue: array){
            if(checkValue==value) return false;
        }
        return true;
    }
}
