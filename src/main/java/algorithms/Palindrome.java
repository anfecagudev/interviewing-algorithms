package algorithms;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;


/**
 * This algorithm determines if a string is a palindrome.
 * Palindromes are words or sentences that when read backwards yield the same word:
 * Example: Anita lava la tina.
 */
public class Palindrome {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please type the word to check if it is a palindrome");
        String text = keyboard.nextLine();
        palindromeChecker(text);

    }

    static void palindromeChecker(final String palindromeProbe) {
        String noSpacesWord = palindromeProbe.replaceAll("\\s+", "");
        List<Character> charList = noSpacesWord.chars().mapToObj(e -> (char) e)
                .collect(Collectors.toList());
        Collections.reverse(charList);
        String result = charList.stream()
                .map(String::valueOf)
                .collect(Collectors.joining());
        if(result.equals(noSpacesWord)) System.out.println(palindromeProbe + " is a Palindrome");
        else System.out.println(palindromeProbe + " is a NOT Palindrome");
    }
}
