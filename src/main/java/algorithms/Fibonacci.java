package algorithms;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/**
 * This algorithm calculates the first n numbers of the fibonacci series.
 */
public class Fibonacci {
    public static void main(String[] args) {
        Scanner data = new Scanner(System.in);
        System.out.println("Set the amount of numbers of the series you want to see");
        int lenght = data.nextInt();
        List<Integer> startingList = new ArrayList<>();
        startingList.add(1);
        startingList.add(1);
        fibonacciCalculator(1,1, startingList, lenght);
    }
    static void fibonacciCalculator(final int lastNumber,
                                   final int prevNumber,
                                   final List<Integer> currentList,
                                   final int length){
        if(currentList.size() == length){
            currentList.forEach(System.out::println);
        } else {
            int newNumber = lastNumber + prevNumber;
            List<Integer> newList = new ArrayList<>(currentList);
            newList.add(newNumber);
            fibonacciCalculator(newNumber, lastNumber, newList, length);
        }
    }
}
