package algorithms;

import java.util.*;
import java.util.stream.Collectors;

public class CountWords {

    public static void main(String[] args) {
        String testString = "The car The bus The dinosaur car bus The watch";
        wordsCounter(testString);
    }

    public static void wordsCounter(final String string){
        List<String> words = Arrays.asList(string.split(" "));
        Map<String, Integer> counter = new HashMap<>();
        words.forEach( word -> {
            if(counter.containsKey(word)){
                Integer times = counter.get(word);
                counter.put(word, times + 1);
            }else {
                counter.put(word, 1);
            }
        });
        Map<String, Integer> sortedMap = counter
                .entrySet()
                .stream()
                .sorted(Map.Entry.<String, Integer> comparingByValue().reversed())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1,e2) -> e1, LinkedHashMap::new));
        sortedMap.forEach((k,v) -> System.out.println("The word " + k + " was found " + v + " times"));

    }
}
