package algorithms;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 * This algorithm determines if a number is prime.
 */
public class PrimeNumber {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Type the number you want to check if is prime");
        int numberToCheck = keyboard.nextInt();
        if (isPrime(numberToCheck)) System.out.println(numberToCheck + " is prime");
        else System.out.println(numberToCheck + " is not prime");
    }

    static boolean isPrime(final int numberToCheck) {
        if (numberToCheck == 0 || numberToCheck == 1) {
            return true;
        }
        int half = numberToCheck / 2;
        List<Integer> dividends = IntStream.rangeClosed(2, half).boxed().collect(Collectors.toList());
        for (int dividend : dividends) {
            if (numberToCheck % dividend == 0) {
                return false;
            }
        }
        return true;
    }
}


