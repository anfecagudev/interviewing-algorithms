package algorithms;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;


/**
 * This algorithm determines if a number is a integer palindrome.
 * Example: 12321 is an integer palindrome because when read backwards yields the same number.
 */
public class IntegerPalindrome {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please type the word to check if it is a palindrome");
        String text = keyboard.nextLine();
        palindromeChecker(text);

    }

    static void palindromeChecker(final String palindromeProbe) {
        List<Character> charList = palindromeProbe.chars().mapToObj(e -> (char) e)
                .collect(Collectors.toList());
        Collections.reverse(charList);
        String result = charList.stream()
                .map(String::valueOf)
                .collect(Collectors.joining());
        if(result.equals(palindromeProbe)) System.out.println(palindromeProbe + " is a Palindrome");
        else System.out.println(palindromeProbe + " is a NOT Palindrome");
    }
}


