package algorithms;

import java.util.ArrayList;
import java.util.List;

/**
 * Have the function FindIntersection(strArr) read the array of strings stored in strArr
 * which will contain 2 elements: the first element will represent a list of comma-separated
 * numbers sorted in ascending order, the second element will represent a second list of
 * comma-separated numbers (also sorted). Your goal is to return a comma-separated
 * string containing the numbers that occur in elements of strArr in sorted order.
 * If there is no intersection, return the string false.
 */
public class ArrayIntersection {
    public static void main(String[] args) {
        String[] input = {"1, 3, 4, 7, 13", "1, 2, 4, 13, 15"};
        String result = FindIntersection(input);
        System.out.printf("Intersection result is: %s \n", result);
        String[] input2 = {"1, 3, 9, 10, 17, 18", "1, 4, 9, 10"};
        String result2 = FindIntersection(input2);
        System.out.printf("Intersection result2 is: %s", result2);
    }

    private static String FindIntersection(String[] strArr){
        String str1 = strArr[0];
        String str2 = strArr[1];
        String[] arr1 = str1.split(",");
        String[] arr2 = str2.split(",");
        List<String> results = new ArrayList<>();
        for(String val : arr1){
            for(String val2 : arr2){
                if(val.equals(val2)){
                    results.add(val);
                    break;
                }
            }
        }
        if(results.isEmpty()) return "False";
        return String.join(",", results);
    }

}


