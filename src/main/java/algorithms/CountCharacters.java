package algorithms;

import java.util.*;
import java.util.stream.Collectors;

public class CountCharacters {

    public static void main(String[] args) {
        String testString = "La palabra amarillo es graciosa";
        charsCounter(testString);
    }

    public static void charsCounter(final String string){
        List<Character> chars = string.chars()
                .mapToObj(e -> (char) e)
                .collect(Collectors.toList());
        Map<Character, Integer> counterMap = new HashMap<>();
        chars.forEach(ch -> {
            if(counterMap.containsKey(ch)) {
                int count = counterMap.get(ch);
                counterMap.put(ch, count+1);
            } else {
                counterMap.put(ch, 1);
            }
        });
        Map<Character, Integer> sortedMap = counterMap
                .entrySet()
                .stream()
                .sorted(Map.Entry.<Character, Integer> comparingByValue().reversed())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        sortedMap.forEach((k,v) -> System.out.println("Char " + k + " was found " + v + " times"));
    }
}
