package algorithms;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.Arrays;

/**
 * This algorithm takes an integers vector as an input and reduces it extracting
 * one integer from another, in such way that the result is the biggest possible.
 */
public class BestSubstractionVector {

    public int[] reducer(final int[] numbers){
        if(numbers.length == 1) {
            System.out.println("The best result possible is " + numbers[0]);
            return numbers;
        } else {
            int[] workingArray = Arrays.copyOf(numbers, numbers.length);
            int length = workingArray.length;
            int dif = 0;
            //ORDERING LOOP
            for (int i = 0; i < length; i++) {
                for (int j = 0; j < length; j++) {
                    if (workingArray[i] > workingArray[j]) {
                        int switcher = workingArray[i];
                        workingArray[i] = workingArray[j];
                        workingArray[j] = switcher;
                    }
                }
            }
            //NEW NUMBER
            dif = workingArray[0] - workingArray[length - 1];
            //NEW ARRAY
            int[] newArray = new int[length - 1];
            System.arraycopy(workingArray, 1, newArray, 0, length - 2);
            newArray[length - 2] = dif;
            //RECURSIVENESS
            return reducer(newArray);
        }
    }

    @Test
    public void reducerTest(){
        int[] testProbe = {3,5,2,-1};
        int[] testProbe2 = {9,-9,8,1,10,-15,6,-2,3};
        int[] result = reducer(testProbe);
        int[] result2 = reducer(testProbe2);
        assertTrue(result[0] == 1);
        assertTrue(result2[0] == 9);

    }
}
