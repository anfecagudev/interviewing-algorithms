package algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This code receives a vector that can be seen as a matrix of a*b
 * For example the vector: [3,4,5,5,3,1,8,1,6,3,2,4,1,4,6,3]
 * Can be seen as:
 *       3   4  5  5
 *       3   1  8  1
 *       6   3  2  4
 *       1   4  6  3
 * Then the algorithm is going to start from every row.
 * If it is possible to go up and to the right, it will go there.
 * If it is possible to go the right, it will go there.
 * If it is possible to go down and to the right, it will go there.
 * It is going to be adding up the amounts on those indexes
 * Finally will determine the route where the sum is the biggest possible.
 */
public class BestVectorRoute {

    private static final List<int[]> routes = new ArrayList<>();

    public static void main(String[] args) {
        int[] array = {3,4,5,5,3,1,8,1,6,3,2,4,1,4,6,3};
        bestRouteCalculator(array, 4);
        sumCalculator(array);
    }

    public static void sumCalculator(final int[] originalVector){
        int finalSum = 0;
        int[] vector = null;
        for(int[] array: routes){
            int internalSum = 0;
            for(int index: array){
                internalSum += originalVector[index];
            }
            if(internalSum > finalSum) {
                finalSum = internalSum;
                vector = array;
            }
        }
        System.out.println("The best value posible is " + finalSum + " and was found in route: ");
        for(int i=0; i < vector.length; i++){
            if(i==0) System.out.print("[" + vector[i]+",");
            else if(i==vector.length-1) System.out.print(vector[i]+"]");
            else System.out.print(vector[i] + ",");
        }

    }

    public static void bestRouteCalculator(final int[] vector, final int rows){
        if(vector.length % rows != 0) throw new RuntimeException("Vector length and number of rows don't match");
        for(int i = 0; i < vector.length/rows; i++){
            int[] route = new int[vector.length/rows];
            route[0]= i*vector.length/rows;
            routeGen(vector, route, 0);
        }
    }

    public static void routeGen(final int[] originalVector,
                          final int[] previousRoute,
                          final int actualIndex){
        if(previousRoute.length == actualIndex+1) routes.add(previousRoute);
        else {
            int rowLength = previousRoute.length;
            int upper = previousRoute[actualIndex] - (rowLength-1);
            int right = previousRoute[actualIndex] + 1 % rowLength;
            int down = previousRoute[actualIndex] + rowLength + 1;
            if( upper > 0) {
                int[] newRoute = Arrays.copyOf(previousRoute, previousRoute.length);
                newRoute[actualIndex+1] = upper;
                routeGen(originalVector, newRoute, actualIndex+1);
            }
            if( right%rowLength < rowLength) {
                int[] newRoute = Arrays.copyOf(previousRoute, previousRoute.length);
                newRoute[actualIndex+1] = right;
                routeGen(originalVector, newRoute, actualIndex+1);
            }
            if( down < originalVector.length){
                int[] newRoute = Arrays.copyOf(previousRoute, previousRoute.length);
                newRoute[actualIndex+1] = down;
                routeGen(originalVector, newRoute, actualIndex+1);
            }
        }

    }
}
