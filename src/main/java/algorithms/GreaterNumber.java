package algorithms;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Find the next greater number that can be created with same digits.
 * For the number 3843, the next greater number possible to be created
 * with same digits is 4338.
 */
public class GreaterNumber {
    public static void main(String[] args) {
        int result = MathChallenge(3843);
        System.out.printf("Next greater number with same digits is: %s.", result);
    }

    static List<int[]> combinations = new ArrayList<>();

    public static int MathChallenge(int num) {
        List<String> numList = Arrays.stream(String.valueOf(num).split("(?!^)")).collect(Collectors.toList());
        int[] indexesArray = new int[numList.size()];
        Arrays.fill(indexesArray, -1000);
        for (int i=0; i<indexesArray.length; i++){
            int[] arrayCopy = new int[indexesArray.length];
            System.arraycopy(indexesArray, 0, arrayCopy, 0, indexesArray.length);
            arrayCopy[0]=i;
            createPosCombinations(arrayCopy, 0);
        }
        List<String> posCombList = combinations.stream()
                .map(arr -> Arrays.stream(arr)
                        .boxed()
                        .map(numList::get)
                        .map(String::valueOf)
                        .collect(Collectors.joining())).distinct().sorted().collect(Collectors.toList());
        int index = posCombList.indexOf(String.valueOf(num));
        if(index<posCombList.size()-1) return Integer.parseInt(posCombList.get(index+1));
        else return -1;
    }

    static void createPosCombinations(int[] numsArray, int index){
        if(numsArray[numsArray.length-1]!=-1000) {
            combinations.add(numsArray);
            return;
        }
        for(int i=0; i<numsArray.length; i++){
            if(!existsInArray(numsArray, i)){
                int[] arrayCopy = new int[numsArray.length];
                System.arraycopy(numsArray,0, arrayCopy, 0, arrayCopy.length);
                arrayCopy[index+1]=i;
                createPosCombinations(arrayCopy, index+1);
            }
        }
    }

    static boolean existsInArray(int[] numsArray, int value){
        for (int j : numsArray) {
            if (j == value) return true;
        }
        return false;
    }


}
