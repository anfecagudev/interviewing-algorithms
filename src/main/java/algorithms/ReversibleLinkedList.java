package algorithms;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/*
 * Algorithm for a LinkedList that can be reversed
 * using both loops and recursion
 */
public class ReversibleLinkedList {
    public static void main(String[] args) {
        CustomLinkedList<String> linkedList = new CustomLinkedList<>();
        linkedList.add("Carlos", null);
        linkedList.add("Diana", Mode.LOOPED);
        linkedList.add("Felipe", Mode.RECURSIVELY);
        linkedList.add("Claudia", Mode.LOOPED);
        linkedList.add("Gonzalo", Mode.RECURSIVELY);
        System.out.println(linkedList.toString());
        System.out.println("==========================");
        linkedList.reverseListLooped();
        System.out.println(linkedList.toString());
        System.out.println("==========================");
        linkedList.reverseListLooped();
        System.out.println(linkedList.toString());
        System.out.println("==========================");
        linkedList.reverseListRecursively();
        System.out.println(linkedList.toString());
        System.out.println("==========================");
        linkedList.reverseListRecursively();
        System.out.println(linkedList.toString());

    }
}

class CustomLinkedList<T>{
    LinkedListNode<T> root;

    public void add(T t, Mode mode){
        if (root == null) {
            root = new LinkedListNode<>(t);
            return;
        }
        if(mode.equals(Mode.RECURSIVELY)){
          addRecursively(root, t);
        } else {
          addLooping(t);
        }
    }

    private void addRecursively(LinkedListNode<T> node, T t) {
        if(node.next == null) {
            LinkedListNode<T> newNode = new LinkedListNode<>(t);
            node.next = newNode;
            newNode.prev = node;
            return;
        }
        addRecursively(node.next, t);
    }

    private void addLooping(T t){
        LinkedListNode<T> currentNode = root;
        while(currentNode.next != null){
            currentNode = currentNode.next;
        }
        LinkedListNode<T> newNode = new LinkedListNode<>(t);
        currentNode.next = newNode;
        newNode.prev = currentNode;
    }

    public  void reverseListRecursively(){
        reverseListRecursively(root);
    }
    private void reverseListRecursively(LinkedListNode<T> node){
        LinkedListNode<T> placeholder = node.next;
        node.next = node.prev;
        node.prev = placeholder;
        if(node.prev == null){
            root = node;
            return;
        }
        reverseListRecursively(node.prev);
    }

    public void reverseListLooped(){
        LinkedListNode<T> node = root;
        while(true){
            LinkedListNode<T> placeholder = node.next;
            node.next = node.prev;
            node.prev = placeholder;
            if(node.prev == null){
                root = node;
                break;
            }
            node = node.prev;
        }
    }
    @Override
    public String toString(){
        List<String> values = new ArrayList<>();
        LinkedListNode<T> currentNode = root;
        while(true){
            values.add(currentNode.value.toString());
            if(currentNode.next == null) break;
            currentNode = currentNode.next;
        }
        return values.stream().collect(Collectors.joining(",", "[", "]"));
    }
}

enum Mode {
    RECURSIVELY, LOOPED
}

class LinkedListNode<T>{
    T value;
    LinkedListNode<T> prev, next;

    LinkedListNode(T value){
        this.value = value;
    }


}
