package algorithms;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * You're going to be provided a List of products {"Product1", "Product2", "Product3"}
 * and a List of Suppliers, each supplier containing a SupplierName and a dictionary of ProductName -> ProductDefinition.
 * ex: {
 *     "Supplier1", ["Product1" -> "price:10, time:2"],
 *     "Supplier1", ["Product1" -> "price:10, time:2"]
 * }
 * Point 1: Create a BestResults object containing the most convenient (cheap) supplier for each product.
 * Total amount of items to buy is 100 of each type.
 * TotalCost is calculated  by price + $1.5 per extra day. Extra days are the difference between purchaseOrder date
 * and DeliverDate
 * Point 2: Consider a mor field in product definition. This states for minimum amount to be ordered. Select supplier
 * if purchased amount is greater that Supplier's mor.
 * Point 3: Consider a Budget field. Print if budget is enough for buying all the indicated products.
 */
public class ProductAndSupplier {

    private static final int AMOUNT = 100;
    private static final double EXTRA_PER_DAY = 1.5;
    private static final double BUDGET = 3200.0;

    public static void main(String[] args) {
        List<String> products = List.of("Product1, Product2, Product3");
        List<Supplier> suppliers = List.of(
                new Supplier("Supplier1", Map.of(
                        "Product1", new ProductDefinition(10, 2, 80),
                        "Product2", new ProductDefinition(8, 3, 110),
                        "Product3", new ProductDefinition(9, 1, 100)
                )),
                new Supplier("Supplier2", Map.of(
                        "Product1", new ProductDefinition(11, 1, 99),
                        "Product2", new ProductDefinition(8, 1, 88),
                        "Product3", new ProductDefinition(10, 2, 25)
                ))
        );
        BestResults bestResults = new BestResults(
                products
                        .stream()
                        .map(prod -> getBestResults(prod, suppliers))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))
        );

        System.out.println("IS BUDGET ENOUGH? : " + isBudgetEnough(bestResults));
    }


    //THIS FUNCTION SOLVES POINT 3
    private static boolean isBudgetEnough(BestResults results){
        return results
                .results()
                .values()
                .stream()
                .map(Result::totalPrice)
                .reduce(0.0, Double::sum) < BUDGET;
    }


    private static AbstractMap.SimpleEntry<String, Result> getBestResults(String product, List<Supplier> suppliers){
        BestSupplierPerProduct bestSupplierPerProduct =
                suppliers
                        .stream()
                        .filter(supp -> supp.products().get(product).mor() <= AMOUNT)  //THIS LINE SOLVES POINT 2
                        .map(supp -> {
                            ProductDefinition productDefinition = supp.products().get(product);
                            Double realValue = EXTRA_PER_DAY * (productDefinition.time() - 1) + productDefinition.price();
                            return new BestSupplierPerProduct(supp.name(), realValue, productDefinition);
                        })
                        .reduce(new BestSupplierPerProduct(null, Double.MIN_VALUE, null),
                                (a,b) -> a.realValue() > b.realValue() ? a : b);
        return new AbstractMap.SimpleEntry<>(product,
                new Result(bestSupplierPerProduct.supplierName(),
                        AMOUNT * bestSupplierPerProduct.realValue(),
                        bestSupplierPerProduct.productDefinition().time()));
    }
}


record BestSupplierPerProduct(String supplierName, Double realValue, ProductDefinition productDefinition){}
record ProductDefinition(int price, int time, int mor){}
record Supplier(String name, Map<String,ProductDefinition> products){}
record Result(String supplier, double totalPrice, int time){}
record BestResults(Map<String, Result> results){}