package algorithms;

import java.util.ArrayList;
import java.util.List;


/**
 * The following algorithm should validate if a word has only unique letters
 * or not. For example "abcdefg" should return true and "abccdefg" should return false;
 */
public class RepeatedWords {
    public static void main(String[] args) {
        System.out.println(isUnique("abcdefg"));
        System.out.println(isUnique("abccdefg"));
    }

    private static boolean isUnique(String str){
        List<Character> characters = new ArrayList<>();
        for(Character character : str.toCharArray()){
            if(characters.contains(character)) return false;
            characters.add(character);
        }
        return true;
    }
}
