package algorithms;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

/**
 * This code receives a vector that can be seen as a matrix of a*b
 * For example the vector: [3,4,5,5,3,1,8,1,6,3,2,4,1,4,6,3]
 * Can be seen as:
 *       3   4  5  5
 *       3   1  8  1
 *       6   3  2  4
 *       1   4  6  3
 * Then the algorithm is going to start from every row.
 * If it is possible to go up and to the right, it will go there.
 * If it is possible to go the right, it will go there.
 * If it is possible to go down and to the right, it will go there.
 * It is going to be adding up the amounts on those indexes
 * Finally will determine the route where the sum is the biggest possible.
 * Approach: Loops.
 */
public class BestVectorRouteLoop {
    public static void main(String[] args) {
        int[] array = {3,4,5,5,3,1,8,1,6,3,2,4,1,4,6,3};
        List<int[]> routes = createRoutes(array, 4);
        Results result = routes.stream()
                .map(route -> {
                    int[] values = Arrays.stream(route)
                            .map(index -> array[index])
                            .toArray();
                    int sum = Arrays.stream(values).sum();
                    return new Results(route, values, sum);
                })
                .reduce(Results.getNullResults(),
                        BinaryOperator.maxBy(Comparator.comparingInt(Results::sum)));
        System.out.println(result);
    }

    public static List<int[]> createRoutes(int[] array, int nCols){
        if(array.length % nCols != 0) throw new RuntimeException("Vector does not constitute a matrix");
        List<int[]> routes = new ArrayList<>();
        int nRows = array.length / nCols;
        for(int i=0; i< nCols; i++){
            int[] row = new int[nCols];
            Arrays.fill(row, Integer.MIN_VALUE);
            row[0] = i * nCols;
            routes.addAll(Objects.requireNonNull(createRoutes(array, row)));
        }
        return routes;
    }

    private static List<int[]> createRoutes(int[] array, int[] initialRow){
        List<int[]> builtRoutes = new CopyOnWriteArrayList<>();
        builtRoutes.add(initialRow);
        toBreak: while(true)  {
            for(int[] route: builtRoutes){
                int activeIndex = findActiveIndex(route);
                if( activeIndex == -1) break toBreak;
                int realIndex = route[activeIndex];
                if(realIndex - route.length > 0)
                    builtRoutes.add(newRoute(route, activeIndex, realIndex - route.length + 1));

                builtRoutes.add(newRoute(route, activeIndex, realIndex + 1));
                if(realIndex + route.length < array.length)
                    builtRoutes.add(newRoute(route, activeIndex, realIndex + route.length + 1));
                builtRoutes.remove(route);
            }
        }
        return builtRoutes;
    }

    private static int[] newRoute(int[] baseArray, int activeIndex, int newValue){
        int[] newArray = new int[baseArray.length];
        System.arraycopy(baseArray, 0, newArray, 0, baseArray.length);
        newArray[activeIndex+1] = newValue;
        return newArray;
    }

    private static int findActiveIndex(int[] route){
        for(int i=0; i<route.length - 1; i++){
            if(route[i+1] == Integer.MIN_VALUE) return i;
        }
        return -1;
    }

}

record Results(int[] route, int[] values, int sum){
    public static Results getNullResults(){
        return new Results(null, null, 0);
    }

    @Override
    public String toString(){
        String route = Arrays.stream(route())
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(",", "[","]"));
        String values = Arrays.stream(values())
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(",", "[","]"));
        return String.format("Route: %s .\nValues: %s .\nSum is: %s.", route, values, sum());
    }
}

