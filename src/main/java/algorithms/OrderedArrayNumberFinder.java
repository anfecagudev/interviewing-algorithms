package algorithms;

import java.util.stream.IntStream;


/**
 * Algorithm to verify if a target number is part of a sorted array of numbers.
 */
public class OrderedArrayNumberFinder {
    public static void main(String[] args) {
        Integer[] integers = IntStream.range(0, 100_000_000)
                .boxed()
//                .filter(num -> num != 26)
                .toArray(Integer[]::new);
        System.out.println(isInArray(integers, 26));



    }

    private static boolean isInArray(Integer[] array, int target){
        int numOfIterations = 0;
        int start = 0;
        int end = array.length;
        while (start < end) {
            numOfIterations++;
            int midpoint = (end + start) / 2;
            if(array[midpoint] == target) {
                System.out.println("Number of iterations: " + numOfIterations);
                return true;
            }
            if(array[midpoint] > target){
                end = midpoint;
            } else {
                start = midpoint + 1;
            }
        }
        System.out.println("Number of iterations: " + numOfIterations);
        return false;
    }
}
