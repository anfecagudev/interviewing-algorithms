package algorithms;

import java.util.Scanner;


/**
 * This algorithm calculates the factorial of a number in two different ways:
 * Using recursiveness and using loops.
 */
public class Factorial {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Number to calculate factorial to");
        int number = keyboard.nextInt();
        System.out.println("The result is " + recursiveFactorial(1, number));
        System.out.println("The result is " + notRecursiveFactorial(number));


    }
    static int recursiveFactorial(final int previousResult, final int missing){
        if (missing == 1){
            return previousResult;
        } else {
            int newResult = previousResult * missing;
            return recursiveFactorial(newResult, missing - 1);
        }
    }
    static int notRecursiveFactorial(final int number){
        int result = 1;
        for(int i = 1; i <= number; i++){
            result *= i;
        }
        return result;
    }

}
