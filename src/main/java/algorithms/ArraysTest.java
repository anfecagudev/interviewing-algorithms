package algorithms;


/**
 * Circular rotate the provided array, starting from the index described in the first
 * element of the initial array.
 * For example, the array {2,3,1,4} will be circular rotated and the first element of the
 * new array will be the element in index 2 of the first one.
 * The final array would be {1,4,2,3}
 */
public class ArraysTest {
    public static int ArrayChallenge(int[] arr) {
        int[] copyArr = new int[arr.length];
        int index=0;
        for(int i=arr[0]; i<arr.length; i++){
            copyArr[index]=arr[i];
            index++;
        }
        for(int i=0; i<arr[0]; i++){
            copyArr[index]=arr[i];
            index++;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for(int num: copyArr){
            stringBuilder.append(num);
        }
        return Integer.parseInt(stringBuilder.toString());
    }

    public static void main (String[] args) {
        int[] arr = {2,3,1,4};
        int result = ArrayChallenge(arr);
        System.out.printf("Result number is: %s", result);
    }
}
