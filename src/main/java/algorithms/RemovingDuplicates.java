package algorithms;

import org.junit.Test;

import java.lang.reflect.Array;
import java.util.Arrays;

import static org.junit.Assert.*;


/**
 * This algorithm removes duplicates from arrays without using Collections api.
 */
public class RemovingDuplicates {
    public <T> T[] duplicateRemover(final T[] array, Class<T> parameterType){
        T[] data = Arrays.copyOf(array, array.length);
        int newSize = array.length;
        for(int i = 0; i < data.length-1; i++){
           for(int j = i+1; j < newSize; j++){
               if(data[i].equals(data[j])) {
                   int shiftToTheLeft = j;
                   for(int k=j+1; k < data.length; k++, shiftToTheLeft++){
                       data[shiftToTheLeft] = data[k];
                   }
                   newSize--;
                   j--;
               }
           }
        }
        T[] newData = (T[]) Array.newInstance(parameterType, newSize);
        for(int i = 0; i<newSize; i++){
            newData[i] = data[i];
        }
        return newData;
    }

    @Test
    public void duplicateTester(){
        String[] data = {"hi","how are you", "hi", "Trying to survive"};
        String[] newData = duplicateRemover(data, String.class);
        assertTrue(newData.length == 3);
        assertEquals("hi", newData[0]);
        assertEquals("how are you", newData[1]);
        assertEquals("Trying to survive", newData[2]);
    }



}
