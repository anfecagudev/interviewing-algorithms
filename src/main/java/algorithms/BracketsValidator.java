package algorithms;


import java.util.EmptyStackException;
import java.util.Map;
import java.util.Stack;

/**
 * Validate if a String made of brackets "(,[,{,},],)" is valid.
 * To be valid it needs to closed with the opposite symbol used to open.
 * For example: () should return true. {) in turn should return false.
 */
public class BracketsValidator {

        private static final Map<String,String> validBracesMap = Map.of("(",")", "[","]", "{","}");


        public static void main(String[] args) {
//            assert validBraces("(){}[]");
//            assert validBraces("([{}])");
//            assert !validBraces("(}");
//            assert !validBraces("[({})](]");
//            assert !validBraces("{[{({[([{}])]})}]}(");
//            assert validBraces("{[{({[([{}])]})}]}");
//            assert validBraces("({(){}()})()({(){}()})(){()}");
//
//
//            assert validBracesWithStack("(){}[]");
//            assert validBracesWithStack("([{}])");
//            assert !validBracesWithStack("(}");
//            assert !validBracesWithStack("[({})](]");
//            assert !validBracesWithStack("{[{({[([{}])]})}]}(");
//            assert validBracesWithStack("{[{({[([{}])]})}]}");
//            assert validBracesWithStack("({(){}()})()({(){}()})(){()}");


            assert validBracesWithStack("({}[])");
            assert !validBracesWithStack("(({()})))");
            assert validBracesWithStack("({(){}()})()({(){}()})(){()}");
            assert !validBracesWithStack("{}()))(()()({}}{}");
            assert !validBracesWithStack("}}}}");
            assert !validBracesWithStack("))))");
            assert !validBracesWithStack("{{{");
            assert !validBracesWithStack("(((");
            assert validBracesWithStack("[]{}(){()}((())){{{}}}{()()}{{}{}}");
            assert validBracesWithStack("[[]][][]");
            assert !validBracesWithStack("}{");
            assert !validBracesWithStack("}");
        }

        private static boolean validBraces(String braces){
            String[] bracesArray = braces.split("(?!^)");
            int startIndex=0;
            for(int i=0; i < bracesArray.length; i++){
                if(startIndex==i && validBracesMap.containsValue(bracesArray[i]) || i==bracesArray.length-1 && validBracesMap.containsKey(bracesArray[i])) return false;
                if(validBracesMap.containsKey(bracesArray[i])) continue;
                if (!validateBraces(bracesArray, startIndex, i)) return false;
                i = i + (i - startIndex - 1);
                startIndex = i + 1;
            }
            return true;
        }

        private static boolean validateBraces(String[] bracesArray, int startIndex, int endIndex){
            try{
                for (int i = startIndex, j = endIndex + (endIndex - startIndex - 1) ; i < endIndex ; i++, j--){
                    if(validBracesMap.get(bracesArray[i]).equals(bracesArray[j])) continue;
                    return false;
                }
            } catch (ArrayIndexOutOfBoundsException e){
                return false;
            }
            return true;
        }

        public static boolean validBracesWithStack(String braces){
            String[] bracesArray = braces.split("(?!^)");
            Stack<String> filterer = new Stack<>();
            for(String symbol : bracesArray){
                if(validBracesMap.containsKey(symbol)) {
                    filterer.push(symbol);
                    continue;
                }
                try {
                    if (!validBracesMap.get(filterer.pop()).equals(symbol)) {
                        return false;
                    }
                } catch (EmptyStackException e){
                    return false;
                }
            }
            return filterer.size() == 0;
        }
}

