package algorithms;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/*
 * Algorithm to capitalize every word in a String.
 * White spaces must be retained.
 */
public class Capitalize {
    public static void main(String[] args) {
        System.out.println(capitalize(""));
        System.out.println(capitalize_long(""));

        System.out.println(capitalize("aaa"));
        System.out.println(capitalize_long("aaa"));

        System.out.println(capitalize("ab cd"));
        System.out.println(capitalize_long("ab cd"));

        System.out.println(capitalize("       ab        cd"));
        System.out.println(capitalize_long("       ab        cd"));

        System.out.println(capitalize("a"));
        System.out.println(capitalize_long("a"));

        System.out.println(capitalize("aa"));
        System.out.println(capitalize_long("aa"));

        System.out.println(capitalize("aaa"));
        System.out.println(capitalize_long("aaa"));

        System.out.println(capitalize("a a"));
        System.out.println(capitalize_long("a a"));

        System.out.println(capitalize("        B         aBB           c    \n          cd               "));
        System.out.println(capitalize_long("        B         aBB           c    \n          cd               "));

        System.out.println(capitalize("        B         abB           c          cd"));
        System.out.println(capitalize_long("        B         abB           c          cd"));

    }

    private static String capitalize(String str){
        StringBuilder stringBuilder = new StringBuilder(str);
        Pattern pattern = Pattern.compile("\\w+");
        Matcher matcher = pattern.matcher(str);
        matcher.results()
                .forEach(matchResult -> stringBuilder.setCharAt(
                        matchResult.start(),
                        Character.toUpperCase(str.charAt(matchResult.start()))));
        return stringBuilder.toString();
    }

    private static String capitalize_long(String string1){
        if(string1.isEmpty()) return "";
        String string2 = string1;
        Pattern pattern = Pattern.compile("\\p{Blank}+");
        Matcher matcher = pattern.matcher(string2);
        boolean firsTime = !String.valueOf(string2.charAt(0)).equals(" ");
        do {
            int end = 0;
            try {
                if(firsTime) firsTime = false;
                else end = matcher.end();
            } catch (IllegalStateException e){
                continue;
            }
            String part1 = string2.substring(0, end);
            String part2 = string2.length() == end ? "" : String.valueOf(string2.charAt(end)).toUpperCase();
            String part3 = string2.length() == end ? "" : string2.substring(end + 1);
            string2 =  part1 + part2 + part3;
        } while (matcher.find());
        return string2;
    }
}
