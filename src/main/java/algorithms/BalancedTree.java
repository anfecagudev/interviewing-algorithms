package algorithms;

import java.util.Objects;

/**
 * AutoBalancing Tree. Requires Java 14+ syntax.
 */

class Main {
    public static void main(String[] args) throws Exception {
        BalancedTree balancedTree = new BalancedTree(1000);
        balancedTree.addValue(500);
        balancedTree.addValue(2000);
        balancedTree.addValue(4000);
        balancedTree.addValue(1500);
        balancedTree.addValue(750);
        balancedTree.addValue(250);
        balancedTree.addValue(100);
        balancedTree.addValue(350);
        balancedTree.addValue(600);
        balancedTree.addValue(850);
        balancedTree.addValue(1250);
        balancedTree.addValue(1750);
        balancedTree.addValue(3000);
        balancedTree.addValue(8000);
        balancedTree.addValue(300);
        balancedTree.addValue(400);
        balancedTree.addValue(325); //FIRST IMBALANCE
        balancedTree.addValue(375);
        balancedTree.addValue(340);
    }
}
public class BalancedTree {

    private Node root;
    public BalancedTree(final int startingValue){
        this.root = new Node(startingValue);
    }

    public void addValue(final int value) throws Exception {
        addValue(root, value);
    }

    private void addValue(Node node, int value) {
        if (value > node.getValue()){
            if(Objects.nonNull(node.getRight())) addValue(node.getRight(), value);
            else {
                addValueLogic(node, Side.RIGHT, value);
            }
        } else {
            if(Objects.nonNull(node.getLeft())) addValue(node.getLeft(), value);
            else{
                addValueLogic(node, Side.LEFT, value);
            }
        }
    }

    private void addValueLogic(Node node, Side side, int value){
        var newNode = new Node(value);
        switch (side){
            case LEFT -> node.setLeft(newNode);
            case RIGHT -> node.setRight(newNode);
        }
        newNode.setPre(node);
        int balance = -1;
        while(balance == -1){
            try {
                balance = checkBalance(this.root);
            } catch (TreeUnbalancedException e){
                System.out.println(e.getMessage());
            }
        }
    }

    private int checkBalance(Node node) throws TreeUnbalancedException {
        var leftHeight = Objects.isNull(node.getLeft()) ? 0 : checkBalance(node.getLeft()) + 1;
        var rightHeight = Objects.isNull(node.getRight()) ? 0 : checkBalance(node.getRight()) + 1;
        if (Math.abs(leftHeight - rightHeight) > 1) {
            balanceNode(node, rightHeight > leftHeight ? Side.RIGHT : Side.LEFT);
            throw new TreeUnbalancedException("Tree was rebalanced");
        }
        return Math.max(leftHeight, rightHeight);
    }


    private void balanceNode(Node node, Side side){
        switch (side){
            case RIGHT -> {
                Node requiredNode = node.getRight();
                while(Objects.nonNull(requiredNode.getLeft())) requiredNode = requiredNode.getLeft();

                if(Objects.nonNull(requiredNode.getRight())){
                    requiredNode.getRight().setPre(requiredNode.getPre());
                    requiredNode.getPre().setLeft(requiredNode.getRight());
                } else requiredNode.getPre().setLeft(null);
                Node preNode = node.getPre();
                if (Objects.nonNull(preNode)){
                    if (preNode.getLeft() == node) {
                        preNode.setLeft(requiredNode);
                    } else {
                        preNode.setRight(requiredNode);
                    }
                    requiredNode.setPre(preNode);
                } else {
                    root = requiredNode;
                    requiredNode.setPre(null);
                }
                node.setPre(requiredNode);
                requiredNode.setLeft(node);
                requiredNode.setRight(node.getRight());
                node.getRight().setPre(requiredNode);
                node.setRight(null);
            }
            case LEFT -> {
                Node requiredNode = node.getLeft();
                while(Objects.nonNull(requiredNode.getRight())) requiredNode = requiredNode.getRight();
                if(Objects.nonNull(requiredNode.getLeft())){
                    requiredNode.getPre().setRight(requiredNode.getLeft());
                    requiredNode.getLeft().setPre(requiredNode.getPre());
                } else requiredNode.getPre().setRight(null);
                var preNode = node.getPre();
                if(Objects.nonNull(preNode)){
                    if (preNode.getLeft() == node) {
                        preNode.setLeft(requiredNode);
                    } else {
                        preNode.setRight(requiredNode);
                    }
                    requiredNode.setPre(preNode);
                } else {
                    root = requiredNode;
                    requiredNode.setPre(null);
                }
                requiredNode.setLeft(node.getLeft());
                node.getLeft().setPre(requiredNode);
                node.setPre(requiredNode);
                requiredNode.setRight(node);
                node.setLeft(null);
            }
        }
    }
}

class TreeUnbalancedException extends Exception{
    public TreeUnbalancedException(String errorMessage){
        super(errorMessage);
    }
}

enum Side{
    LEFT, RIGHT;
}

class Node {
    private Node right;
    private Node left;
    private Node pre;
    private final int value;

    public Node(final int value){
        this.value = value;
        this.right = null;
        this.left = null;
        this.pre = null;
    }

    public int getValue() {
        return value;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getPre() {
        return pre;
    }

    public void setPre(Node pre) {
        this.pre = pre;
    }
}



