package algorithms;


/**
 * Should return the smallest substring containing the letters of requested string.
 * Example. Sample: 'afkidjesldakjesksddl' Substract:'ijesl'
 * Result: 'idjesl'
 */
public class SmallerSubstring {
    public static void main(String[] args) {
        String fullString = "afkidjesldakjiesksddl";
        String letters = "ijesl";
        String result = getSmallestSubstring(fullString, letters);
        System.out.printf("The smallest substring containing %s letters is: %s", letters, result);
    }

    private static String getSmallestSubstring(String fullString, String letters) {
        String smallest = "";
        for(int i=0; i<fullString.codePoints().count(); i++){
            if(letters.contains(String.valueOf(fullString.charAt(i)))){
                StringBuilder stringBuilder = new StringBuilder();
                for(int j=i; j<fullString.codePoints().count(); j++){
                    stringBuilder.append(fullString.charAt(j));
                    if(isCompleted(stringBuilder.toString(), letters)){
                        if (smallest.equals("") || stringBuilder.toString().codePoints().count() <
                                smallest.codePoints().count()) {
                            smallest=stringBuilder.toString();
                        }
                        break;
                    }
                }
            }
        }
        return smallest;
    }

    private static boolean isCompleted(String current, String letters){
        for(char ch: letters.toCharArray()){
            if(!current.contains(String.valueOf(ch))) return false;
        }
        return true;
    }
}
