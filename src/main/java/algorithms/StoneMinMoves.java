package algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/*
 * You have a 3 * 3 grid. You have 9 stones distributed within the grid.
 * Each cell can contain several stones.
 * The objective is to determine the minimum amount of movements, so you get a single stone in each cell.
 * A stone can move across cells by the sides, not by corners.
 */
public class StoneMinMoves {

    private static final AtomicInteger minValue = new AtomicInteger(0);

    public static void main(String[] args) {
        int[][] case1 = {
                {1, 0, 1},
                {1, 3, 0},
                {0, 2, 1}
        };
        int[][] case2 = {
                {1, 3, 0},
                {1, 0, 0},
                {1, 0, 3}
        };
        int[][] case3 = {
                {6, 0, 0},
                {2, 0, 1},
                {0, 0, 0}
        };
        List.of(case1, case2, case3).forEach(StoneMinMoves::processCases);
    }

    private static void processCases(int[][] grid){
        resetCounter();
        validateGrid(grid);
        process(grid);
        printResult(grid);
    }

    private static void process(int[][] grid){
        process(grid, 0);
    }

    private static void process(int[][] grid, int accumulated){
        if(minValue.get() > 0 && accumulated >= minValue.get()) return;
        Pair<List<Pair<Integer,Integer>>, List<Pair<Integer,Integer>>> anomalies = getAnomalies(grid);
        List<Pair<Integer, Integer>> zeroes = anomalies.p1();
        if(zeroes.isEmpty()){
            if(accumulated < minValue.get() || minValue.get() == 0) minValue.set(accumulated);
            return;
        }
        zeroes.forEach(pair -> moveStone(grid, pair, anomalies.p2(), accumulated));
    }

    private static void moveStone(int[][] grid, Pair<Integer,Integer> zeros, List<Pair<Integer,Integer>>bulk, int accumulated){
        bulk.forEach(source -> {
            int[][] newGrid = copyGrid(grid);
            newGrid[zeros.p1()][zeros.p2()] = 1;
            newGrid[source.p1()][source.p2()]--;
            process(newGrid, accumulated +
                    Math.abs(zeros.p1() - source.p1()) +
                    Math.abs(zeros.p2() - source.p2()));
        });
    }

    private static Pair<List<Pair<Integer,Integer>>, List<Pair<Integer,Integer>>> getAnomalies(int[][] grid){
        List<Pair<Integer,Integer>> zeros = new ArrayList<>();
        List<Pair<Integer,Integer>> bulk = new ArrayList<>();
        IntStream.range(0, 3)
                .forEach(rowIndex -> {
                    int[] arr = grid[rowIndex];
                    IntStream.range(0,3)
                            .forEach(colIndex -> {
                                if(arr[colIndex] == 0){
                                    zeros.add(new Pair<>(rowIndex, colIndex));
                                } else if (arr[colIndex] > 1) {
                                    bulk.add(new Pair<>(rowIndex, colIndex));
                                }
                            });
                });
        return new Pair<>(zeros, bulk);
    }

    private static int[][] copyGrid(int[][] currentGrid){
        int[][] newGrid = new int[3][3];
        IntStream.range(0,3).forEach(index -> System.arraycopy(currentGrid[index], 0, newGrid[index], 0, 3));
        return newGrid;
    }

    private static void resetCounter(){
        minValue.set(0);
    }

    private static void validateGrid(int[][] grid){
        Optional<Integer> summedValue =
                Arrays.stream(grid)
                        .flatMapToInt(Arrays::stream)
                        .boxed()
                        .reduce(Integer::sum);
        if(summedValue.isEmpty() || summedValue.get() != 9) throw new IllegalArgumentException("Array does not contain 9 stones");
    }

    private static void printResult(int[][] grid){
        System.out.println("<=============================================================>\nGrid:");
        Arrays.stream(grid)
                .map(arr -> Arrays.stream(arr)
                        .mapToObj(String::valueOf)
                        .collect(Collectors.joining(",", "{", "}")))
                .forEach(System.out::println);
        System.out.println("Min amount of moves is " + minValue.get());
    }
}

record Pair<P1,P2> (P1 p1, P2 p2) { }
