package algorithms;

/**
 * The following algorithm should convert hyphened and dashed words to camelcase.
 * For example: "the-stealth-warrior" should be turned into theStealthWarrior.
 * and A-B-C should be turned into ABC.
 */
public class Camelcase {
    public static void main(String[] args) {
        System.out.println(camelCase("the-stealth-warrior"));
        System.out.println(camelCase("A_B-C"));
    }

    public static String camelCase(String dashed){
        StringBuilder stringBuilder = new StringBuilder();
        String[] words = dashed.split("[-_]");
        stringBuilder.append(words[0]);
        for(int i=1; i<words.length; i++){
            stringBuilder.append(String.valueOf(words[i].charAt(0)).toUpperCase())
                    .append(words[i].substring(1));
        }
        return stringBuilder.toString();
    }
}
