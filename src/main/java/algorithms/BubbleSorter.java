package algorithms;


import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * This algorithm is used to sort arrays in log(n) complexity
 */
public class BubbleSorter {

    private static final AtomicInteger iterations = new AtomicInteger(0);

    public static void main(String[] args) {
        int[] array = {4, 2, 1, 6, 3, 8, 7};
        bubbleSort(array, 0, array.length - 1);
        System.out.printf("Solved in %s iterations. Ordered array: \n", iterations);
        System.out.println(Arrays.stream(array)
                .boxed()
                .map(String::valueOf)
                .collect(Collectors.joining(",", "{", "}")));
    }
    private static void bubbleSort(final int[] array, final int lowIndex, final int upIndex){
        if(upIndex - lowIndex <= 1){
            return;
        }
        int pivot = array[lowIndex];
        int iterUpIndex = upIndex;
        int iterLowIndex = lowIndex;
        while(true){
            if(iterUpIndex == iterLowIndex){
                array[iterLowIndex] = pivot;
                bubbleSort(array, lowIndex, iterLowIndex - 1);
                bubbleSort(array, iterUpIndex + 1, upIndex);
                break;
            }
            iterations.getAndIncrement();
            while(array[iterUpIndex]>= pivot && iterUpIndex > iterLowIndex){
                iterUpIndex--;
            }
            array[iterLowIndex]=array[iterUpIndex];
            if(iterUpIndex - iterLowIndex > 0 ) iterLowIndex++;
            while(array[iterLowIndex] <= pivot && iterLowIndex < iterUpIndex) {
                iterLowIndex++;
            }
            array[iterUpIndex] = array[iterLowIndex];
            if(iterUpIndex - iterLowIndex > 0) iterUpIndex--;
        }
    }
}
