package algorithms;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * AutoBalancing Tree. Requires Java 14+ syntax.
 * Using loops instead of recursion.
 */
class MainLoop{
    public static void main(String[] args) {
        BalancedTreeLoop balancedTree = new BalancedTreeLoop(1000);
        balancedTree.addValue(500);
        balancedTree.addValue(2000);
        balancedTree.addValue(4000);
        balancedTree.addValue(1500);
        balancedTree.addValue(750);
        balancedTree.addValue(250);
        balancedTree.addValue(100);
        balancedTree.addValue(350);
        balancedTree.addValue(600);
        balancedTree.addValue(850);
        balancedTree.addValue(1250);
        balancedTree.addValue(1750);
        balancedTree.addValue(3000);
        balancedTree.addValue(8000);
        balancedTree.addValue(300);
        balancedTree.addValue(400);
        balancedTree.addValue(325); //FIRST UNBALANCE
        balancedTree.addValue(375);
        balancedTree.addValue(340);
    }
}

public class BalancedTreeLoop {
    NodeLoop root;
    public BalancedTreeLoop(int startingValue){
        this.root = new NodeLoop(startingValue);
    }
    public void addValue(int value){
       NodeLoop current = root;
       while(true){
           if(value > current.getValue()){
               if(Objects.isNull(current.getRight())){
                   NodeLoop newNodeLoop = new NodeLoop(value);
                   newNodeLoop.setPre(current);
                   current.setRight(newNodeLoop);
                   break;
               }
               else current = current.getRight();
           }

           if(value < current.getValue()) {
               if(Objects.isNull(current.getLeft())){
                   NodeLoop newNodeLoop = new NodeLoop(value);
                   newNodeLoop.setPre(current);
                   current.setLeft(newNodeLoop);
                   break;
               }
               else current = current.getLeft();
           }
       }
       while(!balanceTree()){}
    }

    public boolean balanceTree(){
        Deque<NodeLoop> nodeStack = new LinkedList<>();
        List<NodeLoop> nodeIterator = new CopyOnWriteArrayList<>();
        nodeIterator.add(root);
        while(!nodeIterator.isEmpty()){
            for(NodeLoop node: nodeIterator){
                nodeStack.push(node);
                if(Objects.isNull(node.getLeft()) && Objects.isNull(node.getRight())) node.setNodePosition(0);
                if(Objects.nonNull(node.getLeft())) {
                    nodeIterator.add(node.getLeft());
                    node.setNodePosition(null);
                }
                if(Objects.nonNull(node.getRight())) {
                    nodeIterator.add(node.getRight());
                    node.setNodePosition(null);
                }
                nodeIterator.remove(node);
            }
        }
        while(!nodeStack.isEmpty()){
            NodeLoop node = nodeStack.pop();
            try{
                node.setNodePosition();
            } catch (TreeUnbalancedException e){
                balanceNode(node);
                return false;
            }
        }
        return true;
    }

    private void balanceNode(NodeLoop node){
        int left = Objects.isNull(node.getLeft()) ? 0 : node.getLeft().getNodePosition();
        int right = Objects.isNull(node.getRight()) ? 0 : node.getRight().getNodePosition();
        if(right > left){
            NodeLoop requiredNode = node.getRight();
            while(Objects.nonNull(requiredNode.getLeft())) requiredNode = requiredNode.getLeft();

            if(Objects.nonNull(requiredNode.getRight())){
                requiredNode.getRight().setPre(requiredNode.getPre());
                requiredNode.getPre().setLeft(requiredNode.getRight());
            } else requiredNode.getPre().setLeft(null);
            NodeLoop preNode = node.getPre();
            if (Objects.nonNull(preNode)){
                if (preNode.getLeft() == node) {
                    preNode.setLeft(requiredNode);
                } else {
                    preNode.setRight(requiredNode);
                }
                requiredNode.setPre(preNode);
            } else {
                root = requiredNode;
                requiredNode.setPre(null);
            }
            node.setPre(requiredNode);
            requiredNode.setLeft(node);
            requiredNode.setRight(node.getRight());
            node.getRight().setPre(requiredNode);
            node.setRight(null);
        }
        else {
            NodeLoop requiredNode = node.getLeft();
            while(Objects.nonNull(requiredNode.getRight())) requiredNode = requiredNode.getRight();
            if(Objects.nonNull(requiredNode.getLeft())){
                requiredNode.getPre().setRight(requiredNode.getLeft());
                requiredNode.getLeft().setPre(requiredNode.getPre());
            } else requiredNode.getPre().setRight(null);
            var preNode = node.getPre();
            if(Objects.nonNull(preNode)){
                if (preNode.getLeft() == node) {
                    preNode.setLeft(requiredNode);
                } else {
                    preNode.setRight(requiredNode);
                }
                requiredNode.setPre(preNode);
            } else {
                root = requiredNode;
                requiredNode.setPre(null);
            }
            requiredNode.setLeft(node.getLeft());
            node.getLeft().setPre(requiredNode);
            node.setPre(requiredNode);
            requiredNode.setRight(node);
            node.setLeft(null);
        }
    }
}

class NodeLoop {
    private NodeLoop right, left, pre;
    private Integer nodePosition;
    private final int value;

    public NodeLoop(final int value){
        this.value = value;
        this.right = null;
        this.left = null;
        this.pre = null;
    }
    public int getValue() {
        return value;
    }
    public NodeLoop getRight() {
        return right;
    }
    public void setRight(NodeLoop right) {
        this.right = right;
    }
    public NodeLoop getLeft() {
        return left;
    }
    public void setLeft(NodeLoop left) {
        this.left = left;
    }
    public NodeLoop getPre() {
        return pre;
    }
    public void setPre(NodeLoop pre) {
        this.pre = pre;
    }
    public Integer getNodePosition() {
        return nodePosition;
    }
    public void setNodePosition(Integer nodePosition) {
        this.nodePosition = nodePosition;
    }
    public void setNodePosition() throws TreeUnbalancedException {
        if(Objects.nonNull(this.nodePosition)) return;
        int left = Objects.isNull(this.getLeft()) ? 0 : this.getLeft().getNodePosition() + 1;
        int right = Objects.isNull(this.getRight()) ? 0 : this.getRight().getNodePosition() + 1;
        if(Math.abs(left - right) > 1) throw new TreeUnbalancedException("Unbalanced tree");
        this.nodePosition = Math.max(left, right);
    }
}
