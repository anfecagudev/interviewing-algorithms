package algorithms;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Find the maximum amount of possible brackets combinations out of the brackets provided by int.
 * If int = 3, for example, you need to find maximum valid bracket combinations out of {}{}{}
 * which in this case are:
 * {{{}}}
 * {{}}{}
 * {}{}{}
 * {}{{}}
 */
public class BracketCombinations {

    static List<int[]> combinations = new ArrayList<>();
    static String[] bracketsArray;
    static List<String[]> bracketCombinations;
    static List<String[]> validBracketCombinations;
    public static void main(String[] args) {
        Set<String> results = calculateBrackets(3);
        System.out.printf("Maximum number of valid bracket combinations: %s. \n", results.size());
        results.forEach(System.out::println);

    }
    static Set<String> calculateBrackets(int numberOfBrackets){
        createPosCombinations(createInitialArray(numberOfBrackets), 0);
        createBracketArray(numberOfBrackets);
        bracketCombinations = combinations
                .stream()
                .map(BracketCombinations::populateBrackets)
                .collect(Collectors.toList());
        validBracketCombinations =
                bracketCombinations.stream()
                        .filter(BracketCombinations::isValidBracketCombination)
                        .collect(Collectors.toList());
        return validBracketCombinations.stream().
                map(arr -> String.join("", arr))
                .collect(Collectors.toSet());
    }

    static int[] createInitialArray(int numberOfBrackets){
        int[] numsArray = new int[numberOfBrackets * 2];
        numsArray[0] = 0;
        return numsArray;
    }

    static void createPosCombinations(int[] numsArray, int index){
        if(numsArray[numsArray.length-1]!=0) {
            combinations.add(numsArray);
            return;
        }
        for(int i=1; i<numsArray.length; i++){
            if(!existsInArray(numsArray, i)){
                int[] arrayCopy = new int[numsArray.length];
                System.arraycopy(numsArray,0, arrayCopy, 0, arrayCopy.length);
                arrayCopy[index+1]=i;
                createPosCombinations(arrayCopy, index+1);
            }
        }
    }

    static boolean existsInArray(int[] numsArray, int value){
        for(int i=1; i<numsArray.length; i++){
            if(numsArray[i]==value) return true;
        }
        return false;
    }

    static void createBracketArray(int numberOfBrackets){
        bracketsArray = new String[numberOfBrackets*2];
        for(int i=0; i<bracketsArray.length; i+=2){
            bracketsArray[i]="{";
            bracketsArray[i+1]="}";
        }
    }

    static String[] populateBrackets(int[] posArray){
        String[] populatedBracket = new String[posArray.length];
        for(int i=0; i<posArray.length; i++){
            populatedBracket[i]=bracketsArray[posArray[i]];
        }
        return populatedBracket;
    }

    static boolean isValidBracketCombination(String[] bracketCombination){
        int left=0;
        int right=0;
        int index=0;
        while(index<bracketCombination.length){
            while(index<bracketCombination.length && bracketCombination[index].equals("{")){
                left++;
                index++;
            }
            while(index<bracketCombination.length && bracketCombination[index].equals("}")){
                right++;
                index++;
            }
            if(right!=left) return false;
            left=0;
            right=0;
        }
        return true;
    }

}
