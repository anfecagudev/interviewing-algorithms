package algorithms;

import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class ReverseString {
    public String reverseUsingString(final String string){
        StringBuilder sb = new StringBuilder();
        sb.append(string);
        sb.reverse();
        return sb.toString();
    }

    public String reverseStringUsingLoops(final String string){
        List<Character> chars = string.chars().mapToObj(e -> (char) e)
                .collect(Collectors.toList());
        Collections.reverse(chars);
        return chars.stream().map(String::valueOf)
                .collect(Collectors.joining());
    }

    @Test
    public void testingMethods(){
        String testString = "Lets Try";
        String result1 = reverseUsingString(testString);
        String result2 = reverseStringUsingLoops(testString);
        assertEquals("yrT steL", result1);
        assertEquals("yrT steL", result2);
    }
}
