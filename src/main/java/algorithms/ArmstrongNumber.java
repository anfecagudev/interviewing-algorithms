package algorithms;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;


/**
 * This algorithm determines if a number is an armstrong number, meaning a number that is the result of the sum of the
 * integers
 * that compounds the number to the power of 3
 * Example 370 = 3^3 + 7^3 + 0^3
 */
public class ArmstrongNumber {
    public static void main(String[] args) {
        Scanner data = new Scanner(System.in);
        System.out.println("Type the number you want to check");
        String numberToCheck = data.nextLine();
        armstrongChecker(numberToCheck);

    }
    static void armstrongChecker(final String numberToCheck){
        Integer number = Integer.valueOf(numberToCheck);
        List<Integer> numbers = numberToCheck.chars()
                .mapToObj(e -> (char) e)
                .map(ch -> Character.getNumericValue(ch))
                .collect(Collectors.toList());
        int calculatedNumber = 0;
        for (int digit: numbers){
            calculatedNumber += Math.pow(digit, 3);
        }
        if(calculatedNumber == number) System.out.println(numberToCheck + " is an armstrong number");
        else  System.out.println(numberToCheck + " is NOT an armstrong number");
    }

}
