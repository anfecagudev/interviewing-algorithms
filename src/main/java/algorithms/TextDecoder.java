package algorithms;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The following algorithm is text decoder.
 * Text will be encoded as follows:
 * Words will be placed backwards.
 * If a letter is repeated in a word, it will be named followed by the number of times the letter is repeated.
 * Example: 'Cas2andra and Man2y' decodes to 'Manny and Cassandra'.
 */
public class TextDecoder {
    public static void main(String[] args) {
        System.out.println(decode("Casa de Man2y IE3 cas2andra logo"));
        System.out.println(decode("Cas2andra and Man2y"));
    }

    private static String decode(String toDecode){
        StringBuilder sb = new StringBuilder(toDecode);
        Pattern pattern = Pattern.compile("\\d+");

        while(true){
            Matcher matcher = pattern.matcher(toDecode);
            if(!matcher.find()) break;
            int number = Character.getNumericValue(toDecode.charAt(matcher.start()));
            char letter = toDecode.charAt(matcher.start()-1);
            String toAdd = String.valueOf(letter).repeat(number);
            sb.replace(matcher.start()-1, matcher.end(), toAdd);
            toDecode = sb.toString();
        }
        return reverseString(sb.toString());
    }

    private static String reverseString(String toReverse){
        String[] words = toReverse.split("\\s+");
        String[] inverted = new String[words.length];

        for(int i=0, j=words.length-1; i< words.length; i++, j--){
            inverted[i] = words[j];
        }
        return String.join(" ", inverted);
    }
}
