package javafeatures;


import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The following algorithm will make use of Reentrant locks
 * to build a Shoe warehouse capable of receiving and fulfilling POs.
 */
public class ShoeWarehouseFulfillmentLock {

    public static void main(String... args) throws InterruptedException {
        var wareHouse = new ShoeWareHouseLock();
        var publisherExecutor = Executors.newFixedThreadPool(2, new WareHousePublisherThreadFactory());
        var consumerExecutor = Executors.newFixedThreadPool(2, new WareHouseConsumerThreadFactory());

        for (int i = 0; i<2; i++){
            publisherExecutor.submit(new Publisher(wareHouse));
            consumerExecutor.submit(new Consumer(wareHouse));
        }
        publisherExecutor.shutdown();
        consumerExecutor.shutdown();
    }
    public static void manual_main(String[] args) throws InterruptedException {
        ShoeWareHouse shoeWareHouse = new ShoeWareHouseLock();
        Thread producer = new Thread(new Publisher(shoeWareHouse), "Producer-1");
        Thread producer2 = new Thread(new Publisher(shoeWareHouse), "Producer-2");
        Thread consumer1 = new Thread(new Consumer(shoeWareHouse), "Consumer-1");
        Thread consumer2 = new Thread(new Consumer(shoeWareHouse), "Consumer-2");
        producer.start();
        producer2.start();
        consumer1.start();
        consumer2.start();

        producer.join();
        producer2.join();
        consumer1.join();
        consumer2.join();

    }
}

class WareHousePublisherThreadFactory implements ThreadFactory {
    private int count = 0;
    @Override
    public Thread newThread(Runnable runnable) {
        String base = "Publisher-";
        return new Thread(runnable, base + ++count);
    }
}

class WareHouseConsumerThreadFactory implements ThreadFactory {
    private int count = 0;

    @Override
    public Thread newThread(Runnable runnable) {
        String base = "Consumer-";
        return new Thread(runnable, base + ++count);
    }
}

class ShoeWareHouseLock extends ShoeWareHouse  {
    private final Lock lock = new ReentrantLock();

    @Override
    public void receiveOrder(Order order) throws Exception {
        try {
            if (!lock.tryLock(3, TimeUnit.SECONDS)) return;
            if (orders.size() == 10) {
                throw new Exception(Thread.currentThread().getName() + " : Queue is full.");
            }
            orders.offer(order);
            System.out.println(order + " has been injected by: " + Thread.currentThread().getName());
        } finally {
            lock.unlock();
        }
    }

    @Override
    public  Order fulfillOrder() throws Exception {
        try {
            while(!lock.tryLock()){
                TimeUnit.SECONDS.sleep(3);
            }
            if(orders.isEmpty()){
                throw new Exception(Thread.currentThread().getName() + " : Queue is empty...");
            }
            Order order = orders.poll();
            System.out.println(Thread.currentThread().getName() + " fulfilled " + order);
            return order;
        } finally {
            lock.unlock();
        }
    }
}

