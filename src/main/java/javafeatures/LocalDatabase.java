package javafeatures;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/*
 * The following code will leverage RandomAccessFile class to build
 * a sort of database, so you can retrieve a particular piece of information
 * from the file without traversing it all.
 * The index will be stored both in the same file and in a different file.
 */
public class LocalDatabase {

    private static final List<String> names = List.of("Juan", "Camilo", "Gonzalo", "Patricia", "Yhon", "Oscar", "Andres");
    private static final List<String> lastNames = List.of("Rodriguez", "Andica", "Goyes", "Cardona", "Zamora", "Jaramillo", "Colino");
    private static final List<Double> salaries = List.of(224.00, 384.00, 231.00, 973.00, 372.87, 803.00, 783.00, 888.00, 273.00);
    private static final List<Student> students;


    static {
        Random random = new Random();
        students = IntStream.range(0, 1000)
                .mapToObj(el -> new Student(el,
                        names.get(random.nextInt(100) % 7),
                        lastNames.get(random.nextInt(100) % 7),
                        random.nextInt(100),
                        salaries.get(random.nextInt(100) % 9)))
                .collect(Collectors.toList());
    }


    public static void main(String[] args) throws IOException {

        //Writing Data
        Scanner scanner = new Scanner(System.in);
        System.out.println("Do you want to use a separate file for indexes? (Y , N)");
        String isSeparate = scanner.next();
        boolean isSeparateIndex = isSeparate.equals("Y");
        writeData(isSeparateIndex);


        //Fetching Data
        Map<Long,Long> indexes = getIndex(isSeparateIndex ? Index.INDEX_FILE.path : Index.DATA_FILE.path);
        try (RandomAccessFile randomAccessFile =
                     new RandomAccessFile(Index.DATA_FILE.path.toFile(), "r")) {
            System.out.println("Type the index of item to find");
            while(scanner.hasNext()){
                long index = scanner.nextLong();
                randomAccessFile.seek(indexes.get(index));
                System.out.println(randomAccessFile.readUTF());
                System.out.println("Type the index of item to find");
            }
        }
    }

    private static Map<Long, Long> getIndex(Path path) throws IOException {
        Map<Long, Long> indexes = new LinkedHashMap<>();
        try (RandomAccessFile randomAccessFile =
                     new RandomAccessFile(path.toFile(), "r")) {
            randomAccessFile.seek(0);
            int numStudents = randomAccessFile.readInt();
            for (int i = 0; i < numStudents; i++) {
                indexes.put(randomAccessFile.readLong(), randomAccessFile.readLong());
            }
            System.out.printf("Loaded %s indexes. \n", numStudents);
        }
        return indexes;
    }

    private static void writeData(boolean isSeparateIndex) throws IOException {
        Map<Long, Long> indexes = new LinkedHashMap<>();

        try (RandomAccessFile randomAccessFile =
                     new RandomAccessFile(Index.DATA_FILE.path.toFile(), "rw")) {
            long startPoint = isSeparateIndex ? 0 : 4 + 16L * students.size();
            randomAccessFile.seek(startPoint);
            for (Student student : students) {
                indexes.put(student.getId(), randomAccessFile.getFilePointer());
                randomAccessFile.writeUTF(student.toString());
            }
            if(isSeparateIndex) writeIndex(indexes, Index.INDEX_FILE.path);
            else writeIndex(indexes, Index.DATA_FILE.path);
        }
    }

    private static void writeIndex(Map<Long,Long> indexes, Path path) throws IOException {
        try (RandomAccessFile randomAccessFile =
                     new RandomAccessFile(path.toFile(), "rw")) {
            randomAccessFile.seek(0);
            randomAccessFile.writeInt(students.size());
            for (Map.Entry<Long, Long> entry : indexes.entrySet()) {
                randomAccessFile.writeLong(entry.getKey());
                randomAccessFile.writeLong(entry.getValue());
            }
        }
    }
}

enum Index {
    DATA_FILE(Path.of("database.db")),
    INDEX_FILE(Path.of("database-index.db"));

    public final Path path;

    Index(Path path) {
        this.path = path;
    }
}

class Student {

    public Student(long id, String name, String lastName, int age, double salary) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.salary = salary;
    }

    private long id;
    private String name, lastName;
    private int age;
    private double salary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                '}';
    }
}
