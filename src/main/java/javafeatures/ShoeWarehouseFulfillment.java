package javafeatures;

import java.util.*;
import java.util.concurrent.TimeUnit;


/**
 * The following algorithm will make use of wait and notify methods
 * to build a Shoe warehouse capable of receiving and fulfilling POs.
 */
public class ShoeWarehouseFulfillment {

    public static void main(String[] args) throws InterruptedException {
        ShoeWareHouse shoeWareHouse = new ShoeWareHouse();
        Thread producer = new Thread(new Publisher(shoeWareHouse), "Producer-1");
        Thread consumer1 = new Thread(new Consumer(shoeWareHouse), "Consumer-1");
        Thread consumer2 = new Thread(new Consumer(shoeWareHouse), "Consumer-2");
        producer.start();
        consumer1.start();
        consumer2.start();

        producer.join();
        consumer1.join();
        consumer2.join();
    }
}

record Order(long id, ShoeType type, int quantity) { }

enum ShoeType{
    TENNIS, FORMAL_SHOE, WOMAN_SHOE;
}

class ShoeWareHouse {
    protected final Queue<Order> orders = new LinkedList<>();

    public synchronized void receiveOrder(Order order) throws Exception {
        while (orders.size() == 10){
            System.out.println("Orders queue is full. Waiting");
            wait();
        }
        orders.offer(order);
        notifyAll();
        System.out.println(order + " has been injected by: " + Thread.currentThread().getName());
    }

    public synchronized Order fulfillOrder() throws Exception {
        while(orders.isEmpty()){
            System.out.println("Thread : " + Thread.currentThread().getName() + " cannot read Order. Queue is empty");
            wait();
        }
        Order order = orders.poll();
        System.out.println(Thread.currentThread().getName() + " fulfilled " + order);
        notifyAll();
        return order;
    }
}

class Publisher implements Runnable {

    private final static Map<Integer, ShoeType> products = Map.of(1, ShoeType.FORMAL_SHOE, 2, ShoeType.WOMAN_SHOE, 3, ShoeType.TENNIS);

    private final ShoeWareHouse wareHouse;

    public Publisher(ShoeWareHouse wareHouse) {
        this.wareHouse = wareHouse;
    }

    @Override
    public void run() {
        Random random = new Random();
        for(int i=0; i<15; i++){
            try {
                Order order = new Order(
                        random.nextLong(1000),
                        products.get(random.nextInt(1,3)),
                        random.nextInt(1,50));
                wareHouse.receiveOrder(order);
                TimeUnit.SECONDS.sleep(random.nextInt(1,3));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        System.out.println(Thread.currentThread().getName() + " closing...");
    }
}

class Consumer implements Runnable {

    private final List<Order> fulfilledOrders = new ArrayList<>();

    private final ShoeWareHouse wareHouse;

    public Consumer(ShoeWareHouse wareHouse) {
        this.wareHouse = wareHouse;
    }

    @Override
    public void run() {
        Random random = new Random();
        while(true){
            try {
                TimeUnit.SECONDS.sleep(random.nextInt(1,3));
                Order order = wareHouse.fulfillOrder();
                fulfilledOrders.add(order);
                if(fulfilledOrders.size() == 5) break;
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        System.out.println(Thread.currentThread().getName() + " fulfilled " + fulfilledOrders.size() + " orders");
        assert fulfilledOrders.size() == 5;
    }

}

